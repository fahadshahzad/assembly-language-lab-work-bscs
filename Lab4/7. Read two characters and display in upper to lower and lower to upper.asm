.model small
.stack 100h 
.data
char1 db "Enter character in uppercase:$"
char2 db "Enter character in lowercase:$"
.code
main proc     
    ; write a code in assembly language to perform the following operations
    ; Read a character in Uppercase
    ; Display the same character in Lower character
    ;---Read  a small letter and display in uppercase
    mov ax, @data           ; we cannt do ax-al because address is of ;2byte so we have to store it in data segmen
    mov ds, ax               ; from where the data segment is opened 
    
     
    mov ah, 09    ;; ------------printing name
    lea dx, char1 ; load address of char1 
    int 21h 
     ;------------------------           
    
    mov ah, 01
    int 21h   
    
    mov bl, al
    
    sub bl,32
    
    mov ah, 02
    mov dl, bl
    int 21h
             
    
    mov ah, 02
    mov dl, 0ah
    int 21h
    
    mov ah, 02
    mov dl, 0dh
    int 21h           
             
             
    mov ah, 09    ;; ------------printing name
    lea dx, char2 ; load address of char1 
    int 21h 
              ;-----------------------------------------------------  
    
    mov ah, 01
    int 21h
       
    mov cl, al
    
    add cl,32
    
    mov ah, 02
    mov dl, cl
    int 21h         
   
    
    mov ah, 4ch
    int 21h
    main endp
end main


