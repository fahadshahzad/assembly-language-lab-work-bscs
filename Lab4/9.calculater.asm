; I am making calculater over here
.model small
.stack 100h 
.data
char1 db "$"
char2 db "$"
.code
main proc     
    mov ax, @data  
    mov ds, ax    
    
     
    mov ah, 09   
    lea dx, char1 
    int 21h 
    
    mov ah, 01
    int 21h   
    
    mov ah, 02
    mov dl, bl
    int 21h
             
    
    mov ah, 02
    mov dl, 0ah
    int 21h
    
    mov ah, 02
    mov dl, 0dh
    int 21h           
             
             
    mov ah, 09    
    lea dx, char2  
    int 21h 
    
    mov ah, 01
    int 21h
       
    mov cl, al
    
    add cl,33  ; will display the next character in lowercase
    
    mov ah, 02
    mov dl, cl
    int 21h         
   
    
    mov ah, 4ch
    int 21h
    main endp
end main


