; loops in assembly
.model small
.stack 100h 
.data
char1 db 65; for A-Z
char2 db 90; for Z-A  
incNumber db 48; for 0-9 
decNumber db 57  ; for 9-0
evenNumbers db 48   ; for printing 0-9 even
decNumbers db 57  ; for printing 9-0 odd numbers 
.code
main proc     
    mov ax, @data
    mov ds, ax   
    ; code to perform the following operations with looping structures
      ;   Display a series of elplabets a-z and all z-a 
      ; display the series of number from 0-9 and 9-0  
      ; to display the odd numbers series upto 9
      ; to display the even series forward and backward series till 9
      
    mov cx,26 ;for loops counter 
    Label1:                                          
        mov ah, 02 
        mov dl, char1 
        int 21h
        
        add char1,1         
    loop Label1  
    ;------------------------------------
    ; space apply
        mov ah, 02
        mov dl, 0ah
        int 21h
        mov ah, 02
        mov dl, 0dh
        int 21h 
    mov cx,26
    Label2:                                          
        mov ah, 02 
        mov dl, char2 
        int 21h
        
        sub char2,1         
    loop Label2
    ;------------------------------------
    ; space apply
        mov ah, 02
        mov dl, 0ah
        int 21h
        mov ah, 02
        mov dl, 0dh
        int 21h 
       
    mov cx,10
    Label3:                                          
        mov ah, 02 
        mov dl, incNumber
        int 21h 
        
        inc incNumber
                
    loop Label3
                                
     ;------------------------------------
    ; space apply
        mov ah, 02
        mov dl, 0ah
        int 21h
        mov ah, 02
        mov dl, 0dh
        int 21h 
       
    mov cx,10
    Label4:                                          
        mov ah, 02 
        mov dl, decNumber
        int 21h 
        
        dec decNumber
                
    loop Label4 
    
    
    
      ;------------------------------------
    ; space apply
        mov ah, 02
        mov dl, 0ah
        int 21h
        mov ah, 02
        mov dl, 0dh
        int 21h 
       
    mov cx,5
    Label5:                                          
        mov ah, 02 
        mov dl, evenNumbers
        int 21h 
        
        add evenNumbers,2
                
    loop Label5 
    
    
      ;------------------------------------
    ; space apply
        mov ah, 02
        mov dl, 0ah
        int 21h
        mov ah, 02
        mov dl, 0dh
        int 21h 
       
    mov cx,5
    Label6:                                          
        mov ah, 02 
        mov dl, decNumbers
        int 21h 
        
        sub decNumbers,2
                
    loop Label6
    
  
    mov ah, 4ch
    int 21h
    main endp
end main


