; loops in assembly
.model small
.stack 100h 
.data
char1 db "Fahad$"
.code
main proc     
    mov ax, @data
    mov ds, ax   
    
    
    
    ;register for counter that is CX-> counter register 
    ; we will create start and end positon
    ; loop will do auto decrement and will transfer the control next
    mov cx,10  ;for loops counter
    Label1:                                          
        mov ah, 09  
        lea dx, char1 
        int 21h 
        ; space apply
        mov ah, 02
        mov dl, 0ah
        int 21h
        mov ah, 02
        mov dl, 0dh
        int 21h      
    loop Label1
    
    
                                ; loops
    
  
    mov ah, 4ch
    int 21h
    main endp
end main


