.model small
.stack 100h
.data
name1 db "Fahad Shahzad$" ; will take 13 byte of memory
phone db "0301-7021045$"   ; 0 is is at address of 01
;  cms dw "051-19-0003"; 0 is at the address upon 02 address  ;;;; take 2 bytes of memory
;  cms dd "051-19-0003"; 0 is at the address upon 04 address;;;; take 4 bytes of memory
.code
main proc
    ; LEA load effective address
    ; String write 09 for String
    mov ax, @data ; we cannt do ax-al because address is of ;2byte so we have to store it in data segmen
    mov ds, ax    ; from where the data segment is opened 
    
     
    mov ah, 09    ;; ------------printing name
    lea dx, name1 ; load address of name1 
    int 21h
    
    ; ------------new line code
    mov ah, 02
    mov dl, 0ah
    int 21h
                
                
    mov ah, 02
    mov dl, 0dh
    int 21h                    
    ; ------------printing phone number
          
    mov ah, 09
    lea dx, phone
    int 21h
    
    
    mov ah, 4ch
    int 21h
    main endp
end main


