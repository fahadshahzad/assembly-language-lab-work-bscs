.model small
.stack 100h
.code
fahad proc
    mov ah, 02
    mov dl, 01110101B   ; by ascii code to print F   
    int 21h   
    
    mov dl, 'N'   
    int 21h  
    
    mov dl, 'I' 
    int 21h
    
    mov dl, 'V' 
    int 21h   
    
    mov dl, 'E'
    int 21h 
    
    mov dl, 'R'
    int 21h
    
    mov dl, 'S'
    int 21h
    
    mov dl, 'I'
    int 21h    
    
    mov dl, 'T'
    int 21h
    
    mov dl, 01111001B
    int 21h
    
    mov ah, 4ch
    int 21h
    
    
    fahad endp  

end fahad

  