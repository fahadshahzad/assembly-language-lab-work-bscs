.model small
.stack 100h
.data
number1 db 49
num1 dw 1
.code
main proc
        mov ax, @data
        mov ds,ax                 
        
        ;*
        ;***
        ;*****
        ;*******
        ;*********
        ;***********
        
        
        mov cx,18
        label1:
            mov bx,cx 
            mov cx,num1               
            label2:   
                mov ah, 02
                mov dl, '*'
                int 21h    
                inc number1    
            loop label2  
            
            add num1,2
            
            mov ah, 02
            mov dl, 0dh
            int 21h
            mov dl, 0ah
            int 21h
            mov number1,49
            mov cx,bx
        loop label1
          
        
        ;_______
        
        mov ax,4ch
        int 21h 
       
        
    main endp
end main
