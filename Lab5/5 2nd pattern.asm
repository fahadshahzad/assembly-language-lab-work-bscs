.model small
.stack 100h
.data
number1 db 49
num1 dw 1
.code
main proc
        mov ax, @data
        mov ds,ax                 
        
        ;1
        ;12
        ;123
        ;1234
        ;12345
        ;123456 
        
        
        mov cx,9
        label1:
            mov bx,cx 
            mov cx,num1               
            label2:   
                mov ah, 02
                mov dl, number1
                int 21h    
                inc number1    
            loop label2  
            
            inc num1
            
            mov ah, 02
            mov dl, 0dh
            int 21h
            mov dl, 0ah
            int 21h
            mov number1,49
            mov cx,bx
        loop label1
        
        
        int 21h  
        
     ;   sub number1,9
        ;_______ 
       
        
    main endp
end main
